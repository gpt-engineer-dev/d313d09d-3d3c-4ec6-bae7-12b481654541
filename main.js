document .getElementById("task-form") .addEventListener("submit", function
(event) { event.preventDefault(); const taskInput =
document.getElementById("task-input"); const taskList =
document.getElementById("task-list"); if (taskInput.value) { const newTask =
document.createElement("li"); newTask.textContent = taskInput.value;
newTask.classList.add( "bg-white", "p-2", "rounded-lg", "flex",
"justify-between", "items-center", ); const completeBtn =
document.createElement("button"); completeBtn.innerHTML = '<i
  class="fas fa-check text-green-500 todo-buttons"
></i
>'; completeBtn.addEventListener("click", function () {
taskList.removeChild(newTask); }); const deleteBtn =
document.createElement("button"); deleteBtn.innerHTML = '<i
  class="fas fa-trash text-red-500"
></i
>'; deleteBtn.addEventListener("click", function () {
taskList.removeChild(newTask); }); newTask.append(completeBtn, deleteBtn);
taskList.appendChild(newTask); taskInput.value = ""; } });
